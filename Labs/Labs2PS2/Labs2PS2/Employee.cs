﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs2PS2
{

    class Employee
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public double BasicSalary { get; set; }
        //public double HRA { get; set; }
        //public double DA { get; set; }
        //public int DepartmentID { get; set; }

        public Employee(int eid,string name,string desi, double ba )
        {
            EmployeeID = eid;
            Name = name;
            Designation = desi;
            BasicSalary = ba;
        }
        
        public void display1()
        {
            Console.WriteLine("....................Employee Details....................");
            double hra = (8 * BasicSalary) / 100;
            double pf = (12 * BasicSalary) / 100;
            double gs = BasicSalary + hra;
            double ns = gs - (hra + pf);
            Console.WriteLine("EmployeeID: "+ EmployeeID);
            Console.WriteLine("Employee Name: "+ Name);
            Console.WriteLine("Designation: "+Designation);
            Console.WriteLine("Basic Salary"+ BasicSalary);
            Console.WriteLine("HRA: "+ hra);
            Console.WriteLine("Provident Funds: "+ pf);
            Console.WriteLine("Gross Salary: "+ gs);
            Console.WriteLine("Net Salary: "+ ns);
        }

    }
}
