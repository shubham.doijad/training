﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs2PS2
{
    class Program
    {
        static void Main(string[] args)
        {
            int flag = 0;
            while (flag != 1)
            {
                //static int id = 0;
                Console.WriteLine("Enter Department Name");
                string dn = Console.ReadLine();
                Console.WriteLine("Enter your location");
                string l = Console.ReadLine();



                Department d = new Department(dn, l);
                Console.WriteLine("Enter your Employee ID");
                int eid = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter your Name");
                string name = Console.ReadLine();

                Console.WriteLine("Enter your Designation as");
                Console.WriteLine("1. Employee");
                Console.WriteLine("2. Manager");
                Console.WriteLine("3. President");
                int val = Convert.ToInt32(Console.ReadLine());
                string desi = null;

                if (val == 1)
                {
                    desi = "Employee";
                }
                if (val == 2)
                {
                    desi = "Manager";
                }
                if (val == 3)
                {
                    desi = "President";
                }
                Console.WriteLine("Enter your Basic Salary");
                double ba = Convert.ToDouble(Console.ReadLine());

                Employee e = new Employee(eid, name, desi, ba);

                d.display();
                e.display1();
                //Console.WriteLine("Department ID: " + id);
                Console.WriteLine("Press 1 if you want to continue else press 0");
                int val1 =Convert.ToInt32( Console.ReadLine());
                if (val1 == 0)
                {
                    flag = 1;
                }

            }
        }
    }
}