﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs2PS2
{
    class Department
    {
        private static int id = 0;
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string Location { get; set; }

        public Department(string dn, string l)
        {
            id = id + 1;
            DepartmentID = id;
            DepartmentName = dn;
            Location = l;


        }

        public void display()
        {
            Console.WriteLine("....................Department Details....................");
            Console.WriteLine("ID: " + DepartmentID);
            Console.WriteLine("Department Name: " + DepartmentName);
            Console.WriteLine("Location: " + Location);
        }

    }
}
