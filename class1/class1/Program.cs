﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class1
{
    class Program
    {
        static void Main(string[] args)
        {
            DrawPolygons(10, 12);
            DrawPolygons(10, 11, 12, 13); 
            Console.ReadLine();

        }
        static void DrawPolygons(params int[] points)
        {
            Console.WriteLine("A polygon of {0} sides..",points.Length);
        }
      
    }
}
